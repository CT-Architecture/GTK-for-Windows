# Licnese

## [License for 7z.exe](http://www.7-zip.org/license.txt)
7z.exe is an executable from www.7-zip.org by Igor Pavlov.

[Source code](http://sourceforge.net/projects/sevenzip/files/7-Zip/9.20/7z920.tar.bz2/download)

## [License for wget.exe](https://www.gnu.org/copyleft/gpl.html)
wget.exe is an executable built by me from the [source code](http://ftp.gnu.org/gnu/wget/wget-1.15.tar.xz) with a macro predefined in host.c.

    #define IN6_ARE_ADDR_EQUAL(a, b) (!memcmp ((const void*)(a), (const void*)(b), sizeof (struct in6_addr)))

## Shell scripts and batch files
They are licensed under the MIT License.

### The MIT License (MIT)

Copyright (c) 2014 Wesley Chan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
